# SunMe

## Install

```bash
vagrant up
bin/minikube start
bin/kubectl apply -f webapp/k8s/deployment.yaml -f webapp/k8s/service.yaml
```

To get the service URL:

```bash
minikube service sunme-webapp-service --url
```
## Docker stuff

Build image:

```bash
docker build -t pisquared/sunme_webapp:latest webapp/
```

Run container:

```bash
docker run -d -p 5000:5000 pisquared/sunme_webapp:latest
```

Push image:

```bash
docker push pisquared/sunme_webapp
```