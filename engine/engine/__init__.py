#!/usr/bin/env python
import os

from flask import Flask

import config

app = Flask(__name__)

app.config['SECRET_KEY'] = 'super-secret'

# override for prod
if os.environ.get("FLASK_WEBAPP_ENV") == "prod":
    import sensitive

    if sensitive.SECRET_KEY == "CHANGEME" \
            or sensitive.SECURITY_PASSWORD_SALT == "CHANGEME":
        raise Exception("Default security key detected, generate more secure ones.")
    app.config['DEBUG'] = False
    app.config['SECRET_KEY'] = sensitive.SECRET_KEY

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['WHOOSHEE_DIR'] = os.path.join(os.getcwd(), 'search_index')


def init_app():
    from engine.store import db, migrate
    db.init_app(app)

    from engine import auth, models_orm
    migrate.init_app(app, db)

    from engine.search import whooshee
    whooshee.init_app(app)

    from engine.views import client_bp
    app.register_blueprint(client_bp)


init_app()
