from engine import app
from pprint import pprint

def test():
  with app.app_context():
    from engine.store import db
    import operator
    from engine.models_orm import Model, Field, FieldType, Filter

    db.create_all()

    Model.create_model(model_name="Task", fields=[
        Field(label="title", type=FieldType.TYPE_SHORT_TEXT),
        Field(label="body", type=FieldType.TYPE_LONG_TEXT),
        Field(label="is_done", type=FieldType.TYPE_CHECKBOX),
    ])

    Model.create_instance(model_name="Task", fields=[
        Field(label="title", value="Create the best productivity system"),
        Field(label="is_done", value="False"),
    ])

    #######################


    pprint("===================================================")
    first_task_by_id = Model.query(model_name="Task", model_id=1)
    pprint("first_task_by_id")
    pprint(first_task_by_id)
    pprint("===================================================")

    first_task_by_filter = Model.query(model_name="Task", filters=[
        Filter("is_done", operator.eq, "False"),
    ])
    pprint("first_task_by_filter")
    pprint(first_task_by_filter)


    none_tasks_are_done = Model.query(model_name="Task", filters=[
        Filter("is_done", operator.eq, "True"),
    ])
    pprint("none_tasks_are_done")
    pprint(none_tasks_are_done)
    pprint("===================================================")

    Model.update(model_name="Task", filters=[
        Filter("is_done", operator.eq, "False")
    ], updates=[
        Field(label="is_done", value="True"),
    ])

    done_tasks = Model.query(model_name="Task", filters=[
        Filter("is_done", operator.eq, "True"),
    ])
    pprint("done_tasks")
    pprint(done_tasks)
    pprint("===================================================")

    none_tasks_are_not_done = Model.query(model_name="Task", filters=[
        Filter("is_done", operator.eq, "False"),
    ])
    pprint("none_tasks_are_not_done")
    pprint(none_tasks_are_not_done)
    pprint("===================================================")


if __name__ == "__main__":
  app.run(host="0.0.0.0")
  test()
