#!/usr/bin/env bash
set -e
export FLASK_APP="webapp.py"
source venv/bin/activate && python -m flask db upgrade