from sqlalchemy_utils import ChoiceType
from engine.model_controller import ModelController, Ownable, Datable
from engine.store import db

FIELD_COUNT = 8

class FieldType(object):
  TYPE_SHORT_TEXT = u"SHORT_TEXT"
  TYPE_LONG_TEXT = u"LONG_TEXT"
  TYPE_INTEGER = u"INTEGER"
  TYPE_TIMER = u"TIMER"
  TYPE_CHECKBOX = u"CHECKBOX"
  TYPE_DATETIME = u"DATETIME"
  TYPE_TIME_PERIOD = u"TIME_PERIOD"
  TYPE_CHOICE_SET = u"CHOICE_SET"
  TYPE_RELATIONSHIP = u"RELATIONSHIP"


class ModelDefinition(db.Model, ModelController, Ownable, Datable):
  TYPE_OPTIONS = [
      (FieldType.TYPE_SHORT_TEXT, u'short_text'),
      (FieldType.TYPE_LONG_TEXT, u'long_text'),
      (FieldType.TYPE_INTEGER, u'integer'),
      (FieldType.TYPE_TIMER, u'timer'),
      (FieldType.TYPE_CHECKBOX, u'checkbox'),
      (FieldType.TYPE_DATETIME, u'datetime'),
      (FieldType.TYPE_TIME_PERIOD, u'time_period'),
      (FieldType.TYPE_CHOICE_SET, u'choice_set'),
      (FieldType.TYPE_RELATIONSHIP, u'relationship'),
  ]

  namespace = db.Column(db.Unicode)
  name = db.Column(db.Unicode)
  inherits = db.Column(db.Unicode)
  next_model_id = db.Column(db.Integer, default=1)

  field_01_label = db.Column(db.Unicode)
  field_01_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_01_options = db.Column(db.Unicode)

  field_02_label = db.Column(db.Unicode)
  field_02_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_02_options = db.Column(db.Unicode)

  field_03_label = db.Column(db.Unicode)
  field_03_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_03_options = db.Column(db.Unicode)

  field_04_label = db.Column(db.Unicode)
  field_04_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_04_options = db.Column(db.Unicode)

  field_05_label = db.Column(db.Unicode)
  field_05_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_05_options = db.Column(db.Unicode)

  field_06_label = db.Column(db.Unicode)
  field_06_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_06_options = db.Column(db.Unicode)

  field_07_label = db.Column(db.Unicode)
  field_07_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_07_options = db.Column(db.Unicode)

  field_08_label = db.Column(db.Unicode)
  field_08_type = db.Column(ChoiceType(TYPE_OPTIONS))
  field_08_options = db.Column(db.Unicode)

  def serialize(self):
    fields = []
    for num in range(1, FIELD_COUNT + 1):
      field = dict()
      label = getattr(self, "field_{:02d}_label".format(num))
      if label is not None:
        field["num"] = "{:02d}".format(num)
        field["label"] = label
        field_type = getattr(self, "field_{:02d}_type".format(num))
        field["type"] = field_type.code
        field["options"] = getattr(self, "field_{:02d}_options".format(num))
        fields.append(field)
    return {
        "name": self.name,
        "inherits": self.inherits,
        "fields": fields,
    }


class ModelInstance(db.Model, ModelController, Ownable, Datable):
  model_definition_id = db.Column(db.Integer,
                                  db.ForeignKey('model_definition.id'))
  model_definition = db.relationship("ModelDefinition",
                                     backref="field_definitions")
  model_id = db.Column(db.Integer)
  field_01_value = db.Column(db.Unicode)
  field_02_value = db.Column(db.Unicode)
  field_03_value = db.Column(db.Unicode)
  field_04_value = db.Column(db.Unicode)
  field_05_value = db.Column(db.Unicode)
  field_06_value = db.Column(db.Unicode)
  field_07_value = db.Column(db.Unicode)
  field_08_value = db.Column(db.Unicode)


# ============= API
# ==============================================================================

from collections import defaultdict
import operator
import json

class Filter(object):
  EQUALS = operator.eq
  LESS_THAN = operator.lt
  GREATER_THAN = operator.gt

  def __init__(self, label=None, comparison=None, value=None):
    self.label = label
    self.comparison = comparison
    self.value = value


class Field(object):
  def __init__(self, label=None, type=None, options=None, value=None):
    self.label = label
    self.type = type
    self.options = options or "{}"
    self.value = value


class Model(object):
  @staticmethod
  def create_model(model_name, fields, model_inherits=None, namespace="default"):
    kwargs = dict()
    for num, field in enumerate(fields):
      num += 1
      assert isinstance(field, Field)
      kwargs["field_{:02d}_label".format(num)] = field.label
      kwargs["field_{:02d}_type".format(num)] = field.type
      kwargs["field_{:02d}_options".format(num)] = field.options

    model_definition = ModelDefinition.create(name=model_name,
                                              namespace=namespace,
                                              inherits=model_inherits,
                                              **kwargs)

  @staticmethod
  def create_instance(model_name, fields, namespace="default"):
    model_definition = ModelDefinition.get_by(name=model_name, namespace=namespace)
    model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                    for num in range(1, FIELD_COUNT + 1)}
    kwargs = dict()
    for field in fields:
      assert isinstance(field, Field)
      if field.label is not None:
        field_num = model_labels.get(field.label)
        if field_num:
          kwargs["field_{:02d}_value".format(field_num)] = field.value

    ModelInstance.create(
          model_id=model_definition.next_model_id,
          model_definition=model_definition,
          **kwargs)
    model_definition.next_model_id += 1
    db.session.add(model_definition)
    db.session.commit()

  @classmethod
  def _filter(cls, model_definition, model_id=None, filters=None):
    model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                    for num in range(1, FIELD_COUNT + 1)}
    if model_id:
      instances = [ModelInstance.get_by(model_definition=model_definition,
                                        model_id=model_id)]
    else:
      assert filters is not None
      resolved_filters = {}
      for filter in filters:
        num = model_labels.get(filter.label)
        resolved_filters["field_{:02d}_value".format(num)] = filter.value
      instances = ModelInstance.get_all_by(model_definition=model_definition,
                                           **resolved_filters)
    return instances

  @classmethod
  def _update_instance(cls, model_definition, instance, updates):
    model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                    for num in range(1, FIELD_COUNT + 1)}
    for field in updates:
      field_num = model_labels.get(field.label)
      if field_num:
        setattr(instance, "field_{:02d}_value".format(field_num), field.value)

    db.session.add(instance)

  @classmethod
  def _serialize_instance(cls, model_definition, instance):
    instance_fields = dict()
    for num in range(1, FIELD_COUNT + 1):
      label = getattr(model_definition, "field_{:02d}_label".format(num))
      if label is not None:
        value = getattr(instance, "field_{:02d}_value".format(num))
        instance_fields[label] = value
    return instance_fields

  @classmethod
  def query(cls, model_name, model_id=None, filters=None):
    model_definition = ModelDefinition.get_by(name=model_name)
    assert model_definition is not None

    instances = cls._filter(model_definition, model_id, filters)

    instances_serialized = list()
    for instance in instances:
      instances_serialized.append(cls._serialize_instance(model_definition, instance))
    return {
        "model": model_definition.serialize(),
        "instances": instances_serialized,
    }

  @classmethod
  def update(cls, model_name, updates, model_id=None, filters=None):
    model_definition = ModelDefinition.get_by(name=model_name)
    assert model_definition is not None

    instances = cls._filter(model_definition, model_id, filters)

    for instance in instances:
      cls._update_instance(model_definition, instance, updates)
    db.session.commit()